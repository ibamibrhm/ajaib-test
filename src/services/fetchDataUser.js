const fetchDataUser = async () => {
  try {
    const resp = await fetch('https://randomuser.me/api/?' + new URLSearchParams({
      inc: 'name,email,gender,registered,login',
      seed: 'ibrahim',
      nat: 'US',
      results: 100,
    }));
    const data = await resp.json();
    return data;
  } catch (error) {
    throw error;
  }
}

export default fetchDataUser;
