import React, { useEffect, useState } from 'react';
import moment from 'moment';
import { Button, Col, Form, Input, notification, Row, Select, Table } from 'antd';
import fetchDataUser from './services/fetchDataUser';

const columns = [
  {
    title: 'Username',
    dataIndex: 'login',
    width: 300,
    render: (row) => row.username,
  },
  {
    title: 'Name',
    dataIndex: 'name',
    width: 300,
    render: (row) => `${row.first} ${row.last}`,
    sorter: (a, b) => {
      const aFullName = `${a.name.first} ${a.name.last}`;
      const bFullName = `${b.name.first} ${b.name.last}`;
      return aFullName.localeCompare(bFullName);
    },
  },
  {
    title: 'Email',
    dataIndex: 'email',
    width: 300,
    sorter: (a, b) => a.email.localeCompare(b.email),
  },
  {
    title: 'Gender',
    dataIndex: 'gender',
    width: 100,
    sorter: (a, b) => a.gender.localeCompare(b.gender),
  },
  {
    title: 'Registered Date',
    dataIndex: 'registered',
    width: 300,
    render: (row) => moment(row.date).format('DD-MM-YYYY HH:MM'),
    sorter: (a, b) => new Date(a.registered.date) - new Date(b.registered.date)
  },
];

const defaultFilters = {
  search: '',
  gender: 'all',
};

const App = () => {
  const [form] = Form.useForm();
  const [dataSource, setDataSource] = useState([]);
  const [loadingTable, setLoadingTable] = useState(false);
  const [filters, setFilters] = useState(defaultFilters);

  useEffect(() => {
    const fetchData = async () => {
      try {
        setLoadingTable(true);
        const data = await fetchDataUser();
        if (filters.gender !== 'all') {
          data.results = data.results.filter((data) => data.gender === filters.gender);
        }
  
        if (filters.search) {
          data.results = data.results.filter((data) => {
            const joinedFields = [data.login.username, data.name.first, data.name.last, data.email].join('');
            return joinedFields.includes(filters.search)
          })
        }
        setDataSource(data.results);
      } catch (error) {
        console.log(error);
        notification.error({ message: 'Fetch data failed' });
      } finally {
        setLoadingTable(false);
      }
    }
    fetchData();
  }, [filters]);

  const onReset = () => {
    form.resetFields();
    setFilters(defaultFilters);
  };

  const handleFiltersChange = (val, field) => {
    setFilters({ ...filters, [field]: val });
  }

  return (
    <React.Fragment>
      <Form layout="vertical" form={form} initialValues={defaultFilters}>
        <Row align="bottom" gutter={10}>
          <Col xs={24} md={5}>
            <Form.Item label="Search" name="search">
              <Input.Search placeholder="Search..." enterButton onSearch={(val) => handleFiltersChange(val, 'search')} />
            </Form.Item>
          </Col>
          <Col xs={24} md={4}>
            <Form.Item label="Gender" name="gender">
              <Select onSelect={(val) => handleFiltersChange(val, 'gender')}>
                <Select.Option value="all">All</Select.Option>
                <Select.Option value="male">Male</Select.Option>
                <Select.Option value="female">Female</Select.Option>
              </Select>
            </Form.Item>
          </Col>
          <Col>
            <Form.Item>
              <Button onClick={onReset}>Reset Filter</Button>
            </Form.Item>
          </Col>
        </Row>
      </Form>

      <Table
        dataSource={dataSource}
        columns={columns}
        rowKey={(row) => row.email}
        loading={loadingTable}
        pagination={{
          pageSize: 5,
          total: dataSource.length,
          showSizeChanger: false,
        }}
        scroll={{ x: 1000 }}
      />
    </React.Fragment>
  );
}

export default App;
