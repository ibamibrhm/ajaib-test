1. Explain how Object Oriented Programming works with a thorough understanding of the keyword this
and the new keyword

```
the "new" keyword is used to create instance class. All property inside class can be accessed with "this".
```

2. What is the new class syntax and how to create instance methods, class methods?

```
class Animal {
    constructor() {

    }
}

const animal = new Animal()
```

3. Give an example of how to implement inheritance in ES2015 using extends and super

```
class Human {
    constructor() {
      this.hands = 2;
      this.legs = 2;
      this.nose = 1;
    }
}

class Male extends Human {
    constructor() {
      super();
      this.gender = 'male';
    }
}

const maleHuman = new Male();
```

4. Imagine refactoring an ES5 application to use ES2015, how would you go about it?

```
- change var to let or const
- change for-loop to forEach/map/filter/reduce depends on what the loop for. This will make code more readable
- destructuring object and array
- implements class
```

5. Give an example of how you structure applications with design patterns using closure and modules

```
function houseMaker(type, localTax = 0) {
    const houseType = {
        small: 15000,
        medium: 50000,
        large: 130000,
    }

    function countPrice() {
        return houseType[type] + (houseType[type] * (localTax/100));
    }

    return countPrice;
}

const smallHouse = houseMaker('medium', 10);
const price = smallHouse();
```

6. What are your preferred ways of testing your web application?

```
Unit test and QA test
```

7. Which web server do you use? Why? Explain pros and cons of your choice.

```
I have no clue for this. I use Amazon Elastic Container Service.
```

8. What is your preferred production deployment process?

```
with CI/CD pipeline
```

9. Give an example of clean README.md documentation.

# PROJECT-MARKDOWN-EXAMPLE
## Usage

Run this command to running the apps:
  ```
  npm install
  npm start
  ```

# List Routes
## User Routes
| Route        | HTTP | Header(s)  | Body                                                  | Response                                                                                                                             |
|--------------|------|------------|-------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------|
| /users       | POST | none       | name: String <br> email: String <br> password: String | **Success:** <br> 201: Object User <br> **Error:** <br> 400: Validation Error <br> 500: Internal server error                        |
| /users/login | POST | none       | email: String <br> password: String                   | **Success:** <br> 200: Access token, Object User <br> **Error:** <br> 500: Internal server error <br> 400: Username / Password wrong |
| /users       | GET  | token:user | none                                                  | **Success:** <br> 200: Object User <br> **Error:** <br> 500: Internal server error                                                   |
| /users       | PUT  | token:user | none                                                  | **Success:** <br> 200: Object User <br> **Error:** <br> 500: Internal server error                                                   |

## Product Routes
| Route                        | HTTP   | Header(s)   | Body                                                                                                               | Response                                                                                                                                                           |
|------------------------------|--------|-------------|--------------------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| /products                    | GET    | none        | none                                                                                                               | **Success:** <br> 200: Array of object products <br> **Error:** <br>  500: Internal server error                                                                   |
| /products                    | POST   | token:admin | **Form Data** <br> title: String <br> description: String <br> stock: Number <br> image: String <br> price: Number | **Success:** <br> 200: Object Product <br> **Error:** <br> 500: Internal server error <br> 400: Validation error                                        |
| /products/:id                | PUT    | token:admin | **Form Data** <br> title: String <br> description: String <br> stock: Number <br> image: String <br> price: Number | **Success:** <br> 200: Object Product <br> **Error:** <br> 500: Internal server error                                                                              |
| /products/:id/addToCart      | PUT    | token:user  | none                                                                                                               | **Success:** <br> 200: Added to your cart <br> **Error:** <br> 500: Internal server error <br> 400: Product out of stock <br> 400: You have this item on your cart |
| /products/:id/removeFromCart | PUT    | token:user  | none                                                                                                               | **Success:** <br> 200: Object User <br> **Error:** <br> 500: Internal server error                                                                                 |
| /products/:id/               | DELETE | token:admin | none                                                                                                               | **Success:** <br> 200: Object Product <br> **Error:** <br> 500: Internal server error <br> 400: Id not found                                                       |

## Transaction Routes
| Route         | HTTP | Header(s)   | Body                                                                                                         | Response                                                                                                                                  |
|---------------|------|-------------|--------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------|
| /transactions | GET  | token:admin | none                                                                                                         | **Success:** <br> 200: Array of object transactions <br> **Error:** <br> 500: Internal server error                                       |
| /transactions | POST | token:user  | name: String <br> address: String <br> city: String <br> state: String <br> zip: String <br> country: String | **Success:** <br> 200: Object Transaction <br> **Error:** <br> 500: Internal server error <br> 400: You don't have any item in your carts |